﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{
    public  interface IScreenState
    {
        ScreenStateManager ScreenStateManager { get; set; } // this is set on the Add method of  ScreenStateManager
        void Draw( GameTime gameTime);
        void Activate(bool b);
        void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate);
    }
}