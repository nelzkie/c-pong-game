﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{
    public class Background : GameEntity
    {


        private Texture2D netBg;
        private Vector2 netbgPosition;
        public override void Activate(ScreenStateManager screenStateManager, bool b)
        {
            base.Activate(screenStateManager, b);
            texture = content.Load<Texture2D>("background1");
            netBg = content.Load<Texture2D>("backgroundMiddle");
            position = new Vector2(0,0);
            netbgPosition = new Vector2(0, 0);
        }


        public override void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {
            
        }

        public override void Draw(GameTime gameTime)
        {
            
            spriteBatch.Draw(texture,viewportBounds,Color.White);   // putting a rectangle bounds in our case the viewport bounds will resize the image automatically to fit it
            
            spriteBatch.Draw(netBg,viewportBounds,Color.White); // putting a rectangle bounds in our case the viewport bounds will resize the image automatically to fit it
        }
    }
}
