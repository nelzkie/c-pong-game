﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

namespace My_Own_Menu_State_System_1
{
    public  class Sounds
    {
        private SoundEffect select,back,win,score,paddlehit,wallbounce;
        private ScreenStateManager screenStateManager;
        private ContentManager content;
        public void Activate(ScreenStateManager screenStateManager, bool b)
        {
            if (b)
            {
                this.screenStateManager = screenStateManager;
                content = new ContentManager(screenStateManager.Game.Services, "Content");
            }
            select = content.Load<SoundEffect>("sounds/Select");
            back = content.Load<SoundEffect>("sounds/back");
            win = content.Load<SoundEffect>("sounds/win");
            paddlehit = content.Load<SoundEffect>("sounds/paddlehit");
            wallbounce = content.Load<SoundEffect>("sounds/wallbounce");
            score = content.Load<SoundEffect>("sounds/playerScore");

        }

        public void SelectPlay()
        {
            select.Play();
        }
        public void BackPlay()
        {
            back.Play();
        }
        public void WinPlay()
        {
            win.Play();
        }
        public void ScorePlay()
        {
            score.Play();
        }

        public void PaddleHit()
        {
            paddlehit.Play();
        }

        public void Wallbounce()
        {
            wallbounce.Play();
        }


    }
}