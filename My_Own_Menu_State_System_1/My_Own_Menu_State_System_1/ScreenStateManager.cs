﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{

    public enum ScreenState
    {
        Play,
        Pause,
        Resume,
        Menu,
        Quit
    }

    public enum Level
    {
        Easy,
        Hard
    }
    public class ScreenStateManager :DrawableGameComponent
    {
        List<IScreenState> screens = new List<IScreenState>();
        List<IScreenState> tmpScreens = new List<IScreenState>();
        public SpriteBatch SpriteBatch;
        public SpriteFont SpriteFont;
        private Texture2D texture;
        public KeyboardState newState, oldState;
        public Sounds Sounds = new Sounds();
        public ScreenState gameplayState = ScreenState.Play;

        public  int escCounter = 0; // coutner for pausing
        public Level dificulty { get; set; }

        public ScreenStateManager(Game game) : base(game)
        {
        }

        protected override void LoadContent()
        {
           SpriteBatch = new SpriteBatch(GraphicsDevice);
            ContentManager content = Game.Content;
            SpriteFont = content.Load<SpriteFont>("font");
            Sounds.Activate(this,true);
            
            foreach (var screenState in screens)
            {
                screenState.Activate(true);
            }
        }

        public override void Update(GameTime gameTime)
        {
            newState = Keyboard.GetState();
            foreach (var screenState in screens)
            {
                tmpScreens.Add(screenState);
            }

            while (tmpScreens.Count > 0)
            {
                IScreenState screen = tmpScreens[tmpScreens.Count - 1];  // will give the last item on the tmpscreen
                tmpScreens.RemoveAt(tmpScreens.Count - 1);
                screen.Update(gameTime, newState, oldState);
            }
            oldState = newState;
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch.Begin();
            foreach (IScreenState screenState in screens)
            {
                screenState.Draw(gameTime);
            }
            SpriteBatch.End();
        }

        public void Add(IScreenState screenState)
        {
            screenState.ScreenStateManager = this;
            screenState.Activate(true);
            screens.Add(screenState);
        }

        public void RemoveScreen(IScreenState screenState)
        {
            
            screens.Remove(screenState);
            tmpScreens.Remove(screenState);
        }
    }
}
