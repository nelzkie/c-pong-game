﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace My_Own_Menu_State_System_1
{
    public abstract class GameScreen
    {
        public ScreenStateManager ScreenStateManager { get; set; }
        
        public virtual void Draw(GameTime gameTime)
        {
            
        }
    }
}
