﻿using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{
    public class Keyhandler
    {

        public bool Evaluate(KeyboardState state, Keys key)
        {
            if (state.IsKeyDown(key))
            {
                return true;
            }
            return false;
        }


    }
}