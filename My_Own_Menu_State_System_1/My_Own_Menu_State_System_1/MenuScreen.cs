﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{
    public class MenuScreen : IScreenState
    {

        
        List<MenuEntry>menuEntries = new List<MenuEntry>();
        private MenuEntry playEntry,optionEntry, exitEntry;
        private int selectedEntry = 0;
        private SpriteBatch spriteBatch;
        private SpriteFont font;
        private float transitionPosition = 1;
        private int direction = -1;
        
        public MenuScreen()
        {
            playEntry = new MenuEntry("Play");
            optionEntry = new MenuEntry("Options");
            exitEntry = new MenuEntry("Exit");

            
            menuEntries.Add(playEntry);
            menuEntries.Add(optionEntry);
            menuEntries.Add(exitEntry);

            // Attaching events
            playEntry.SelectInputHandler += PlayEntry_SelectInputHandler;   // this means that the selecthandler is pointing to the method PlayEntry_SelectInputHandler
            optionEntry.SelectInputHandler += OptionEntry_SelectInputHandler;


     
        }

        private void OptionEntry_SelectInputHandler(object sender, EventArgs e)
        {
            Debug.WriteLine("Option");
            ScreenStateManager.RemoveScreen(this);
            ScreenStateManager.Add(new OptionMenuScreen());
        }

        /// <summary>
        /// The method to be attached on the event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void PlayEntry_SelectInputHandler(object sender, EventArgs e)
        {
            Debug.WriteLine("Play");
            ScreenStateManager.gameplayState = ScreenState.Play;
            ScreenStateManager.RemoveScreen(this);
            ScreenStateManager.Add(new GamePlayManager());
        }



        public void Activate(bool b)
        {
            // no use so far
            Debug.WriteLine(ScreenStateManager.dificulty);
        }

        public void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {
            if(state.IsKeyDown(Keys.Up) && oldstate.IsKeyUp(Keys.Up))
            {
                selectedEntry--;
                ScreenStateManager.Sounds.SelectPlay(); // play the select sound
                if (selectedEntry < 0) selectedEntry = 0;
            }
            if (state.IsKeyDown(Keys.Down) && oldstate.IsKeyUp(Keys.Down))
            {
                selectedEntry++;
                ScreenStateManager.Sounds.SelectPlay(); // play the select sound
                if (selectedEntry > menuEntries.Count - 1) selectedEntry = menuEntries.Count - 1;
            }
            for (int i = 0; i < menuEntries.Count; i++)
            {
                
                menuEntries[i].Update(gameTime, i == selectedEntry);
            }



            // NOTE: To understand this carefully you need to understand how events and delegate works.
            if (state.IsKeyDown(Keys.Enter) && oldstate.IsKeyUp(Keys.Enter))
            {
                ScreenStateManager.Sounds.BackPlay();
                menuEntries[selectedEntry].OnSelectedEntry();
            }
        }

        public ScreenStateManager ScreenStateManager { get; set; }

        public  void Draw(GameTime gameTime)
        {
            float transitionDelta = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000;
            transitionPosition += transitionDelta * direction;
            if (transitionPosition >= 1 || transitionPosition <= 0) // check for transitionposition
            {
                //If you dont know what does clamp do, look it up on google
                //https://msdn.microsoft.com/en-us/library/microsoft.xna.framework.mathhelper.clamp.aspx
                transitionPosition = MathHelper.Clamp(transitionPosition, 0, 1); // we need to clamp the transitionPosition because if we dont, the transition will just keep on going and going.
            }


            float transitionOffset = (float)Math.Pow(transitionPosition, 4); // this will make the the transition slow down as it nears the end. Its called power curve
            Vector2 position = new Vector2(100,100);    // this is the whole position of the menu text on the screen


            // this is where the slide animation happens
            for (int i = 0; i < menuEntries.Count; i++)
            {
                position.X = 300;
                position.X -= transitionOffset*150;
                position.Y += menuEntries[i].getHeight(this);
                menuEntries[i].Position = position;
                menuEntries[i].Draw(gameTime, i == selectedEntry, this);
            }
        }

       
    }
}
